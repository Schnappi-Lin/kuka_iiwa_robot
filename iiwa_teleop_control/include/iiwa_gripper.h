#ifndef _IIWA_GRIPPER
#define _IIWA_GRIPPER

#include"ros/ros.h"         
#include"std_msgs/Float64MultiArray.h" //gazebo simulator
#include "Config.h" 

enum GripperState{
    closed=0,
    open,
    uncertain
};
class IIWA_Gripper
{
    public:
        // IIWA_Gripper(ros::NodeHandle&); //initialize

        bool Init(ros::NodeHandle&);  
        bool opengripper();
        bool closegripper();

        GripperState gripper_state = open;
    private:

        bool init_flag;
        
        ros::ServiceClient real_gripper; //real robot
        ros::Publisher sim_gripper; //sim_robot

};

#endif