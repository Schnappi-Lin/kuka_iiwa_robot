#ifndef _IIWA_ROS_NODE_
#define _IIWA_ROS_NODE_

#include <iostream>
#include"ros/ros.h"
#include "iiwa_msgs/JointPosition.h"
#include "iiwa_msgs/JointPositionVelocity.h"
#include "iiwa_msgs/CartesianPose.h"
#include "iiwa_msgs/JointVelocity.h"
#include "iiwa_msgs/SetSpeedOverride.h"
#include "iiwa_msgs/SetSmartServoLinSpeedLimits.h"
#include "trajectory_msgs/JointTrajectory.h"
#include "trajectory_msgs/JointTrajectoryPoint.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/TwistStamped.h"
#include <iiwa_gripper.h>
using namespace std;



class IIWA_ROS_NODE
{
    public:
        //node handle
        ros::NodeHandle iiwa; 

        IIWA_ROS_NODE();
        void IIWA_JointVelocity_Send(float* goal_joints);
void IIWA_JointPositionVelocity_Send(float* goal_joints);

        void IIWA_CartesianVelocity_Send(float* goal_cart);

        void IIWA_Jointposition_Send(float* goal_joints);
        void IIWA_Cartesianpose_Send(float* goal_cart);
        void TrajectoryJoint_Send(float* goal_joints, float duration_t);

        bool SetSpeedOverride(float override);
        bool SetSmartServoSpeedLimit(float limit);
        //user variable
        IIWA_Gripper gripper;
        
        float cur_cart_pose[6];
        sensor_msgs::JointState real_joint_state, sim_joint_state;

         ros::Time begin_t,pre_t;
         float test_vlocity;
    private:
        // geometry_msgs::Pose real_cart_pose, sim_cart_state;
        int Send_flag,counter=0;
        
        //Publisher
        ros::Publisher iiwa_JointV;
        ros::Publisher iiwa_CartV;

        ros::Publisher iiwa_pub_Jp;
        ros::Publisher iiwa_pub_Cp;
        ros::Publisher iiwa_pub_JpV;

        ros::Publisher joint_trajectory_pub;

        ros::Publisher TEST2;
        ros::Time latency_frame_check;
        //client
        ros::ServiceClient iiwa_SetSpeedOverride;
        ros::ServiceClient iiwa_SetCartVelocity;
        //Subscriber
        ros::Subscriber sub_sim_joint,sub_real_joint;
        ros::Subscriber sub_sim_cart, sub_real_cart,test;

        void Real_CartesianPoseCallback(iiwa_msgs::CartesianPose cartesian_pose);
        void Real_JointStatesCallback(iiwa_msgs::JointPosition joint_states);
        void Sim_JointStatesCallback(sensor_msgs::JointState joint_states);
        void Sim_CartesianPoseCallback(geometry_msgs::PoseStamped cartesian_pose);
        void testlatency(iiwa_msgs::JointVelocity test_v);
};
#endif