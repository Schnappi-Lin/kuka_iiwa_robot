#ifndef _IIWA_KINEMATIC
#define _IIWA_KINEMATIC

struct Quaternion {
    float w, x, y, z;
};

struct EulerAngles {
    double roll, pitch, yaw;
};

Quaternion ToQuaternion(double yaw, double pitch, double roll);
EulerAngles ToEulerAngles(Quaternion q);

bool Inverse_Solver(float* goalpose,float*joints);

#endif