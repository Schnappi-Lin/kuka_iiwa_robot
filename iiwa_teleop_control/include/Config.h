#ifndef _CONFIG_
#define _CONFIG_

#define Real_Robot
#define Simulation_Robot

//Joint Position Mode
#define Default_Joints_Step_size  10  //degree
#define Joint_step_step_size 1        //degree

//Joint Velocity Mode
#define Joints_Vel_step 0.1    // rad/s

//Cartesian Position Mode
#define Default_Cart_Step_size  (0.017*4) // linear:m   orient:rad
#define Cart_step_step_size 0.017   

#endif
