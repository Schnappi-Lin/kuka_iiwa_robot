#include"ros/ros.h"
#include<iostream>

#include <chrono>
#include <string.h>
#include <math.h>

#include "sensor_msgs/Joy.h"
#include <iiwa_ros_node.h>
#include <iiwa_kinematic.h>

using namespace std;
//Joints
#define Joints_Step_size  10.0   // Degree
#define Joints_P_v  20        // degree/s

#define Joints_V_init 1.0    // degree/s
#define Joints_V_step 0.5    // degree/s
#define Joints_V_MAX  40     // degree/s

//Cartesian
#define Cart_Step_size  0.017 // m
#define Cart_P_v  0.1  // m/s

#define Cart_V_init 0.1  // m/s
#define Cart_V_step 0.01 // m/s
#define Cart_V_Max  1    // m/s

#define Deg2Rad(x) (x)*(M_PI/180)
#define Rad2Deg(x) (x)*(180/M_PI)
#define SIGN(x) (((x)>0) ? 1.0 : -1.0)
enum Space{
    JOINT_SPACE,
    CARTESIAN_SPACE,
};
enum Mode{
    POSITION_MODE,
    VELOCITY_MODE
};
enum JointsObject{
  A1=0,
  A2,
  A3,
  A4,
  A5,
  A6,
  A7
};
enum CartesianObject{
    x=0,
    y,
    z,
    Roll,Pitch,Yaw,

    Cartesian_Position, //x y, z
    Cartesian_Angle     //Roll,Pitch,Yaw,
};
struct Control_State{
  Space space;
  Mode mode;
  JointsObject joints_object;
  CartesianObject cart_object;
};
enum Send_Command{
  Send_Joints,
  Send_Cartesian_Pose,
  Gripper_open,
  Gripper_close,
  Not_Send
};
struct Goal_State{
  Send_Command send_cmd;
  float joints_goal[7];
  float cart_goal[6];
};



class IIWA_Joystick_Controler
{
  private:
    const float jointlimit[7]={168,118,168,118,168,118,173};
    vector<float> joy_axes;

    int pre_key=0;
    float cur_joint_v = Joints_V_init;
    float cur_cart_v = Cart_V_init;
    IIWA_ROS_NODE* iiwa_ros_node;

  public:
    Control_State control_state;
    Goal_State goal_state;

    float duration_t=0;
    
  IIWA_Joystick_Controler(IIWA_ROS_NODE* n){ 
    iiwa_ros_node = n;
    control_state.space = JOINT_SPACE;
    control_state.mode = POSITION_MODE;
    control_state.joints_object = A1;
    control_state.cart_object = x;

    goal_state.send_cmd = Not_Send;
    };

  string getNameofControlledObject()
  {
    string ss;
    switch(control_state.space)
    {
      case JOINT_SPACE:
        ss ="Joint_A"+ to_string(control_state.joints_object+1);
        break;

      case CARTESIAN_SPACE:
        switch(control_state.cart_object)
        {
          case x: ss= "x-axis direction";break;
          case y: ss= "y-axis direction";break;
          case z: ss= "z-axis direction";break;
          case Roll: ss= "Roll orientation";break;
          case Pitch: ss= "Pitch orientation";break;
          case Yaw: ss= "Yaw orientation";break;
          default: ss="cartesian degree is not intial before using";
        }
        break;
      default:
          ss="something need to be Intial !";
    }
    return ss;
  }
  
  void SetCurJointAsGoal(){
      
        for(int i =0;i<7;i++){
          #ifdef Real_Robot 
            goal_state.joints_goal[i] = iiwa_ros_node->real_joint_state.position[i]; 
          #else
            goal_state.joints_goal[i] =  iiwa_ros_node->sim_joint_state.position[i];
          #endif 
        }
     
  }
  void SetCurCartPoseAsGoal(){
      for(int i=0;i<6;i++)
      {
          goal_state.cart_goal[i]=iiwa_ros_node->cur_cart_pose[i];
      }
    
  }
 
  float Joints_PositionLimit_Check(float value_in_rad)
  {
    int object = control_state.joints_object;
      if( value_in_rad >=Deg2Rad(jointlimit[object]))
          return Deg2Rad(jointlimit[object]);
     else if (value_in_rad <=Deg2Rad(-jointlimit[object]))
        return Deg2Rad(-jointlimit[object]);
      else 
        return value_in_rad;
  }
  float Joints_Velocitylimit_Check(float vel)
  {
    if(vel>=Joints_V_MAX)
      return Joints_V_MAX;
    else 
      return vel; 
  }
  
  void Joints_Position_Control()
  {
      static float pre_value = 0;
      int object = control_state.joints_object;
      SetCurJointAsGoal();
      if(abs(round(joy_axes[2]))) //deadzone is -0.5 to 0.5
      {
        goal_state.joints_goal[object]= Joints_PositionLimit_Check( goal_state.joints_goal[object]+Deg2Rad(Joints_Step_size)*SIGN(joy_axes[2]) );
        duration_t= Joints_Step_size/Joints_P_v;
        cout<<"goal posistion is : "<< Rad2Deg(goal_state.joints_goal[object])<<"\n";
        goal_state.send_cmd = Send_Joints;
        pre_value = joy_axes[2];
      }
      else if(pre_value!=0) // stop
      {
          goal_state.send_cmd = Send_Joints;                            
          duration_t=0.05;
          pre_value =0;
      }

  }
  void Joints_Velocity_Control()
  {
    static float pre_value = 0;
    int object = control_state.joints_object;
    SetCurJointAsGoal();
    if(joy_axes[2]>0.05||joy_axes[2]<-0.05) //deadzone is -0.05 to 0.05
    {
        cur_joint_v = Joints_V_MAX*abs(joy_axes[2]);
        duration_t= abs(jointlimit[object]*SIGN(joy_axes[2]) - Rad2Deg(goal_state.joints_goal[object]) )/cur_joint_v;
        goal_state.joints_goal[object]= Deg2Rad(jointlimit[object])*SIGN(joy_axes[2]);

        cout<< "curent joint speed is: "<<cur_joint_v<<"\n"; 
        goal_state.send_cmd = Send_Joints;
        pre_value = joy_axes[2];
    }
    else if(pre_value!=0) // stop
    {
        goal_state.send_cmd = Send_Joints;                            
        duration_t=0.05;
        pre_value =0;
        cur_joint_v= Joints_V_init;  //reset velocity  
    }  
      
  }
  void Joints_Control_Mode()
  {
      switch (control_state.mode)
      {    
        case POSITION_MODE:
              Joints_Position_Control();
              break; 
        case VELOCITY_MODE:
              Joints_Velocity_Control();
              break;     
        default:
          cout<<"some error happen\n";
          break;
      }
  }
  

  float Cart_Velocitylimit_Check(float vel)
  {
    if(vel>=Cart_V_Max)
      return Cart_V_Max;
    else 
      return vel; 
  }
  void Cartesian_Position_Control()
  {
      static bool pre_value=0;
      int object = control_state.cart_object;
      switch (control_state.cart_object)
      {
      case Cartesian_Position:  //x y z
        if(abs(joy_axes[0])>0.1||abs(joy_axes[1])>0.1||abs(joy_axes[2])>0.1)
        {
            goal_state.cart_goal[x] += Cart_Step_size*(joy_axes[1]); //p_x 
            goal_state.cart_goal[y] += Cart_Step_size*(joy_axes[0]); //p_y
            goal_state.cart_goal[z] += Cart_Step_size*(joy_axes[2]); //p_z

            if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal))
            {
              goal_state.cart_goal[x] -= Cart_Step_size*(joy_axes[1]);  //undo
              goal_state.cart_goal[y] -= Cart_Step_size*(joy_axes[0]);  //undo
              goal_state.cart_goal[z] -= Cart_Step_size*(joy_axes[2]);  //undo           
              cout<<"limits Hit!!!"<<endl;
            }
            else
            {
              goal_state.send_cmd = Send_Cartesian_Pose;
              // duration_t=Cart_Step_size*max(max(abs(joy_axes[0]),abs(joy_axes[1])),abs(joy_axes[2]))/Cart_P_v;
              duration_t= max(max(abs(goal_state.cart_goal[x]-iiwa_ros_node->cur_cart_pose[x]),
                                  abs(goal_state.cart_goal[y]-iiwa_ros_node->cur_cart_pose[y])),
                                  abs(goal_state.cart_goal[z]-iiwa_ros_node->cur_cart_pose[z]))/Cart_P_v; 
              cout<<"current cartesian goal is\n x :"<<goal_state.cart_goal[x]<<"\n y:"<<goal_state.cart_goal[y]
                  <<"\n z:"<<goal_state.cart_goal[z]<<"\n";
              
            } 
            pre_value =1;
        }
        else if(pre_value)
        {
          // #ifdef Real_Robot
          //     SetCurJointAsGoal();   
          //     goal_state.send_cmd = Send_Joints;                            
          //     duration_t=0.05; 
          //     cur_cart_v = Cart_V_init;  
          // #endif   
          pre_value =0;
        }
   
        break;
      case Cartesian_Angle: //Roll Pitch Yaw
        if(abs(joy_axes[0])>0.1||abs(joy_axes[1])>0.1||abs(joy_axes[2])>0.1)
        {
            goal_state.cart_goal[Roll] += Cart_Step_size*(joy_axes[0]); 
            goal_state.cart_goal[Pitch] += Cart_Step_size*(joy_axes[1]);
            goal_state.cart_goal[Yaw] += Cart_Step_size*(joy_axes[2]);

            if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal))
            {
              goal_state.cart_goal[Roll] -= Cart_Step_size*(joy_axes[0]);  //undo
              goal_state.cart_goal[Pitch] -= Cart_Step_size*(joy_axes[1]);  //undo
              goal_state.cart_goal[Yaw] -= Cart_Step_size*(joy_axes[2]);  //undo           
              cout<<"limits Hit!!!"<<endl;
            }
            else
            {
              goal_state.send_cmd = Send_Cartesian_Pose;
              // duration_t=Cart_Step_size*max(max(abs(joy_axes[0]),abs(joy_axes[1])),abs(joy_axes[2]))/Cart_P_v;
              duration_t= max(max(abs(goal_state.cart_goal[Roll]-iiwa_ros_node->cur_cart_pose[Roll]),
                                  abs(goal_state.cart_goal[Pitch]-iiwa_ros_node->cur_cart_pose[Pitch])),
                                  abs(goal_state.cart_goal[Yaw]-iiwa_ros_node->cur_cart_pose[Yaw]))/Cart_P_v; 
              cout<<"current cartesian goal is\n Roll :"<<Rad2Deg(goal_state.cart_goal[Roll])<<"\n Pitch:"<<Rad2Deg(goal_state.cart_goal[Pitch])
                  <<"\n Yaw:"<<Rad2Deg(goal_state.cart_goal[Yaw])<<"\n";
              
            } 
            pre_value =1;
        }
        else if(pre_value)
        {
          // #ifdef Real_Robot
          //     SetCurJointAsGoal();   
          //     goal_state.send_cmd = Send_Joints;                            
          //     duration_t=0.05; 
          //     cur_cart_v = Cart_V_init;  
          // #endif   
          pre_value =0;
        }
   
        break;
     
      default:
        break;
      }
   
        
  }
  void Cartesian_Velocity_Control()
  {
      static bool pre_value=0;
      int object = control_state.cart_object;
      switch (control_state.cart_object)
      {
      case Cartesian_Position:  //x y z
        if(abs(joy_axes[0])>0.1||abs(joy_axes[1])>0.1||abs(joy_axes[2])>0.1)
        {
            goal_state.cart_goal[x] += Cart_Step_size*(joy_axes[1]); //p_x 
            goal_state.cart_goal[y] += Cart_Step_size*(joy_axes[0]); //p_y
            goal_state.cart_goal[z] += Cart_Step_size*(joy_axes[2]); //p_z

            if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal))
            {
              goal_state.cart_goal[x] -= Cart_Step_size*(joy_axes[1]);  //undo
              goal_state.cart_goal[y] -= Cart_Step_size*(joy_axes[0]);  //undo
              goal_state.cart_goal[z] -= Cart_Step_size*(joy_axes[2]);  //undo           
              cout<<"limits Hit!!!"<<endl;
            }
            else
            {
              goal_state.send_cmd = Send_Cartesian_Pose;           
              cur_cart_v = Cart_V_Max*max(max(abs(joy_axes[0]),abs(joy_axes[1])),abs(joy_axes[2]));
              duration_t= max(max(abs(goal_state.cart_goal[x]-iiwa_ros_node->cur_cart_pose[x]),
                                  abs(goal_state.cart_goal[y]-iiwa_ros_node->cur_cart_pose[y])),
                                  abs(goal_state.cart_goal[z]-iiwa_ros_node->cur_cart_pose[z]))/cur_cart_v; 
              cout<<"current cartesian goal is\n x :"<<goal_state.cart_goal[x]<<"\n y:"<<goal_state.cart_goal[y]
                  <<"\n z:"<<goal_state.cart_goal[z]<<"\n";
              
            } 
            pre_value =1;
        }
        else if(pre_value)
        {
          #ifdef Real_Robot
              SetCurJointAsGoal();   
              goal_state.send_cmd = Send_Joints;                            
              duration_t=0.05; 
          #endif   
          cur_cart_v = Cart_V_init;
          pre_value =0;
        }
   
        break;
      case Cartesian_Angle: //Roll Pitch Yaw
        if(abs(joy_axes[0])>0.1||abs(joy_axes[1])>0.1||abs(joy_axes[2])>0.1)
        {
            goal_state.cart_goal[Roll] += Cart_Step_size*(joy_axes[0]); 
            goal_state.cart_goal[Pitch] += Cart_Step_size*(joy_axes[1]);
            goal_state.cart_goal[Yaw] += Cart_Step_size*(joy_axes[2]);

            if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal))
            {
              goal_state.cart_goal[Roll] -= Cart_Step_size*(joy_axes[0]);  //undo
              goal_state.cart_goal[Pitch] -= Cart_Step_size*(joy_axes[1]);  //undo
              goal_state.cart_goal[Yaw] -= Cart_Step_size*(joy_axes[2]);  //undo           
              cout<<"limits Hit!!!"<<endl;
            }
            else
            {
              goal_state.send_cmd = Send_Cartesian_Pose;
              cur_cart_v = Cart_V_Max*max(max(abs(joy_axes[0]),abs(joy_axes[1])),abs(joy_axes[2]));
              duration_t= max(max(abs(goal_state.cart_goal[Roll]-iiwa_ros_node->cur_cart_pose[Roll]),
                                  abs(goal_state.cart_goal[Pitch]-iiwa_ros_node->cur_cart_pose[Pitch])),
                                  abs(goal_state.cart_goal[Yaw]-iiwa_ros_node->cur_cart_pose[Yaw]))/cur_cart_v; 
              cout<<"current cartesian goal is\n Roll :"<<Rad2Deg(goal_state.cart_goal[Roll])<<"\n Pitch:"<<Rad2Deg(goal_state.cart_goal[Pitch])
                  <<"\n Yaw:"<<Rad2Deg(goal_state.cart_goal[Yaw])<<"\n";
              
            } 
            pre_value =1;
        }
        else if(pre_value)
        {
          #ifdef Real_Robot
              SetCurJointAsGoal();   
              goal_state.send_cmd = Send_Joints;                            
              duration_t=0.05; 
          #endif   
          cur_cart_v = Cart_V_init; 
          pre_value =0;
        }
   
        break;
     
      default:
        break;
      }
  
  }
  void Cartesian_Control_Mode()
  {
      switch (control_state.mode)
      {    
        case POSITION_MODE:
              Cartesian_Position_Control();
              break; 
        case VELOCITY_MODE:
              Cartesian_Velocity_Control();
              break;     
        default:
          cout<<"some error happen\n";
        break;
      }
  }
  
  void Control_Updata()
  { 
    switch (control_state.space)
    {
      case JOINT_SPACE:
        Joints_Control_Mode();
        break;
      case CARTESIAN_SPACE:
        Cartesian_Control_Mode();
        break;
     
      default:
        break;
    }
  }

  void Update_ControlMode(vector<float> axes,vector<int> buttons)
  {
    if(axes[3]>0.5) //Joint space
    {
      control_state.space = JOINT_SPACE;
        if(buttons[2]==1)
        {
          control_state.mode=POSITION_MODE;
          cout<<"Joint Space, Position control Mode:\n";
        }
        else if(buttons[3]==1)
        {
          control_state.mode=VELOCITY_MODE;
          cout<<"Joint Space, Velocity control Mode:\n";
        }      
    }
    else if(axes[3]<-0.5) //Cartesian space
    {
      control_state.space = CARTESIAN_SPACE;
      SetCurCartPoseAsGoal();
      if(buttons[2]==1)
        {
          control_state.mode=POSITION_MODE;
          cout<<"Cartesian Space, Position control Mode:\n";
        }
        else if(buttons[3]==1)
        {
          control_state.mode=VELOCITY_MODE;
          cout<<"Cartesian Space, Velocity control Mode:\n";
        } 
    }  
  }
  void Update_ControlObject(vector<int> buttons)
  {
    switch ( control_state.space)
    {
    case JOINT_SPACE:
        if(buttons[5]==1)
          control_state.joints_object = A1;
        if(buttons[6]==1)
          control_state.joints_object = A2;
        if(buttons[7]==1)
          control_state.joints_object = A3;
        if(buttons[8]==1)
          control_state.joints_object = A4;
        if(buttons[9]==1)
          control_state.joints_object = A5;
        if(buttons[10]==1)
          control_state.joints_object = A6;
        if(buttons[11]==1)
          control_state.joints_object = A7;
        cout<<"Current control Joint is : "<<getNameofControlledObject()<<"\n";
        break;

    case CARTESIAN_SPACE:
        if(buttons[4]==1)
        {
            control_state.cart_object = Cartesian_Position;
            cout<<"Current control Objects are : X Y Z \n";
        }
        if(buttons[5]==1)
        {
          control_state.cart_object = Cartesian_Angle;
          cout<<"Current control Objects are : Roll,Pitch,Yaw\n";
        }
        break;
    default:
      cout<<"something wrong!\n";
      break;
    }
  }

/*---------------------------------------------
  Only get called when the data of joy changes
----------------------------------------------*/
  void JoyStickCallback(sensor_msgs::Joy joy)
  {
    joy_axes = joy.axes;
    Update_ControlMode(joy.axes,joy.buttons); //update the control mode
    Update_ControlObject(joy.buttons);        //update the selected object
    
    if(joy.buttons[0]==1)  
    {
      switch (iiwa_ros_node->gripper.gripper_state)
      {
        case closed:
          iiwa_ros_node->gripper.opengripper();
          break;
        case open:
          iiwa_ros_node->gripper.closegripper();
          break;
        default:
          break;
      }
    }
    Control_Updata();
  }

};


int main(int argc, char **argv)
{
  ros::init(argc, argv, "joystick_controller");
  IIWA_ROS_NODE iiwa_ros_node;
  IIWA_Joystick_Controler iiwa_joystick_controler(&iiwa_ros_node); 
  ros::Subscriber iiwa_joy = iiwa_ros_node.iiwa.subscribe("joy", 1 ,&IIWA_Joystick_Controler::JoyStickCallback,&iiwa_joystick_controler);

  ROS_INFO("start reading joystick....");
  ros::Rate loop_rate(100);
  while (ros::ok())
  {
  
    ros::spinOnce();  //renew the Current_cartesian_pose
    
    // iiwa_joystick_controler.Control_Updata();  //commad process

    switch (iiwa_joystick_controler.goal_state.send_cmd)
    {
      case Send_Joints:
          iiwa_ros_node.IIWA_Jointposition_Send(iiwa_joystick_controler.goal_state.joints_goal);
          iiwa_ros_node.TrajectoryJoint_Send(iiwa_joystick_controler.goal_state.joints_goal,iiwa_joystick_controler.duration_t);
          iiwa_joystick_controler.goal_state.send_cmd = Not_Send;
          break;
      case Send_Cartesian_Pose:
          iiwa_ros_node.IIWA_Cartesianpose_Send(iiwa_joystick_controler.goal_state.cart_goal); 
          iiwa_ros_node.TrajectoryJoint_Send(iiwa_joystick_controler.goal_state.joints_goal,iiwa_joystick_controler.duration_t);
          iiwa_joystick_controler.goal_state.send_cmd = Not_Send;
          break;
     
      default:
        break;
    }

    loop_rate.sleep();
  }
    return 0;
}