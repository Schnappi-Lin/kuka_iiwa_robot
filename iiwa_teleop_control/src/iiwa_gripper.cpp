#include<iostream>
#include"ros/ros.h"
#include "iiwa_msgs/SetSpeedOverride.h"
#include <iiwa_gripper.h>

using namespace std;


bool IIWA_Gripper::Init(ros::NodeHandle& iiwa){
    init_flag = false;
    //for real robot
    real_gripper = iiwa.serviceClient<iiwa_msgs::SetSpeedOverride>("/iiwa/command/toolGripper"); 
    // for sim robot
    sim_gripper = iiwa.advertise<std_msgs::Float64MultiArray>("/iiwa/GripperJointInterface_controller/command",1);
  

    init_flag = true;
    return init_flag;
}

bool IIWA_Gripper::opengripper(){
     

    //sim gripper
    // std_msgs::Float64MultiArray msg;
    // std_msgs::MultiArrayDimension dim;
    // dim.size =2;
    // dim.stride =1;
    // msg.layout.dim.push_back(dim);
    // msg.data.push_back(0);
    // msg.data.push_back(0);
    // sim_gripper.publish(msg);
    // gripper_state=open;

    #ifdef Real_Robot
        iiwa_msgs::SetSpeedOverride grippercontrol;
        grippercontrol.request.override_reduction = 1;

        if(real_gripper.call(grippercontrol)){
            if(grippercontrol.response.success)
            {
                ROS_INFO("successful open gripper!");
                gripper_state = open;
                return true;
            }		
            else
            {
                ROS_INFO("fail to open gripper");
            }
        }
        else{
            ROS_INFO("fail to call gripper server");
        }
        return false;
    #endif

    return 1;
}

bool IIWA_Gripper::closegripper(){
    
    //sim gripper
    // std_msgs::Float64MultiArray msg;
    // std_msgs::MultiArrayDimension dim;
    // dim.size =2;
    // dim.stride =1;
    // msg.layout.dim.push_back(dim);
    // msg.data.push_back(0.035);
    // msg.data.push_back(-0.035);
    
    // sim_gripper.publish(msg);
    // gripper_state = closed;

    #ifdef Real_Robot
        iiwa_msgs::SetSpeedOverride grippercontrol;
        grippercontrol.request.override_reduction = 0;
        if(real_gripper.call(grippercontrol)){
            if(grippercontrol.response.success){
                ROS_INFO("successful close gripper!");
                gripper_state = closed;
                return true;
            }		
            else{
                ROS_INFO("fail to close gripper");    
            }
        }
        else{
            ROS_INFO("fail to call gripper server");
        }
        return false;
    #endif
    return true;
}