#include "iiwa_kinematic.h"
#include <math.h>
#include <iostream>
/*---------------------------------
   change EulerAngle to Quanternion
------------------------------------*/
Quaternion ToQuaternion(double yaw, double pitch, double roll) // yaw (Z), pitch (Y), roll (X)
{
    // Abbreviations for the various angular functions
    double cy = cos(yaw * 0.5);
    double sy = sin(yaw * 0.5);
    double cp = cos(pitch * 0.5);
    double sp = sin(pitch * 0.5);
    double cr = cos(roll * 0.5);
    double sr = sin(roll * 0.5);

    Quaternion q;
    q.w = cy * cp * cr + sy * sp * sr;
    q.x = cy * cp * sr - sy * sp * cr;
    q.y = sy * cp * sr + cy * sp * cr;
    q.z = sy * cp * cr - cy * sp * sr;

    return q;
}
/*---------------------------------
   change Quanternion to EulerAngle
------------------------------------*/
EulerAngles ToEulerAngles(Quaternion q) {
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles.roll = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = 2 * (q.w * q.y - q.z * q.x);
    if (abs(sinp) >= 1)
        angles.pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        angles.pitch = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles.yaw = atan2(siny_cosp, cosy_cosp);

    return angles;
}
//vector norm
double vector_norm(float*a,int m)
{
  double sum=0;
  for(int i =0;i<m;i++)
  {
    sum+=a[i]*a[i];
  }
  return sqrt(sum);
}
//vector dot 
double vector_dot(float*a, float*b,int n)
{
	double sum = 0;
	for (int i = 0; i < n; i++)
	{
		sum += a[i] * b[i];
	}
	return sum;
}
//matrix multiply  mat_a*mat_b
//size of matrix a is: n_row * m
//size of matrix b is: m * n_col
void matrix_multiply(float*mat_a,float*mat_b,float* result,int n_row,int n_col,int m) 
{
	int i, j, k;
	for (i=0;i<n_row;i++)
	{
		for (j=0;j<n_col;j++)
		{
			result[i*n_col + j] = 0; //intial
			for (k = 0; k < m; k++)
			{
				result[i*n_col + j] += mat_a[i*m+k] * mat_b[j+k*n_col];
			}		
		}
	}
}
void matrix_minus(float*a, float*b,float*result,int row,int column)
{
  for(int i=0;i<row;i++)
  {
    for(int j=0;j<column;j++)
        result[i*column+j]=a[i*column+j]-b[i*column+j];
  }
}
void matrix_add(float*a, float*b,float*result,int row,int column)
{
  for(int i=0;i<row;i++)
  {
    for(int j=0;j<column;j++)
        result[i*column+j]=a[i*column+j]+b[i*column+j];
  }
}
//matrix transpose 
//row and column is the size of matrix a
void Transpose(float*a, float*a_t, int row, int column)
{
	for(int i=0;i<row;i++)
		for (int j = 0; j < column; j++)
			a_t[j*row+i] = a[i*column+j];
}
//From a quaternion to an orthogonal matrix
void Quaternion_Matrix(float* quaternion,float*m_r) 
{
  int i;
  double norm_q=  vector_dot(quaternion,quaternion,4);
  float outer_q[4][4]={0};
  for(i=0;i<4;i++)
  {
    quaternion[i]=quaternion[i]*sqrt(2.0/norm_q);
  }
  matrix_multiply(quaternion,quaternion,outer_q[0],4,4,1);
  m_r[0]= 1.0-outer_q[1][1]-outer_q[2][2];
  m_r[1]= outer_q[0][1]-outer_q[2][3];
  m_r[2]= outer_q[0][2]+outer_q[1][3];

  m_r[3]= outer_q[0][1]+outer_q[2][3];
  m_r[4]= 1.0-outer_q[0][0]-outer_q[2][2];
  m_r[5]= outer_q[1][2]-outer_q[0][3];

  m_r[6]= outer_q[0][2]-outer_q[1][3];
  m_r[7]= outer_q[1][2]+outer_q[0][3];
  m_r[8]= 1.0-outer_q[0][0]-outer_q[1][1];    
}
void rr(float*p,float*ty,float*tz)
{
  *ty = atan2(sqrt(p[0]*p[0]+p[1]*p[1]),p[2]);
  *tz = atan2(p[1],p[0]);
  if(*tz < -M_PI/2.0)
  {
    *ty = -*ty;
    *tz +=M_PI;
  }
  else if(*tz > M_PI/2.0)
  {
    *ty = -*ty;
    *tz -= M_PI;
  }
}

void Rz(float tz, float result[])
{
  float cz=cos(tz),sz=sin(tz);
  result[0] = cz;
  result[1] = -sz;
  result[2] = 0;

  result[3] = sz;
  result[4] = cz; 
  result[5] = 0.0;
  
  result[6] = 0;                
  result[7] = 0;
  result[8] = 1;                   
}
void Ryz(float ty, float tz, float result[])
{
  float cy = cos(ty),sy = sin(ty),cz=cos(tz),sz=sin(tz);
  result[0] = cy * cz;
  result[1] = -sz;
  result[2] = sy * cz;

  result[3] = cy * sz;
  result[4] = cz; 
  result[5] = sy * sz;
  
  result[6] = -sy;                
  result[7] = 0;
  result[8] = cy;    

}
/*---------------------------------
   kinemetic inverse calculation
-----------------------------------*/
bool Inverse_Solver(float* goalpose,float*joints)
{
  static float tool_length =0;  
  static float L02 = 0.34;
  static float L24 = 0.4;
  static float L46 = 0.4;
  static float L6E = 0.126 + tool_length;
  static float Tr = 0.0;
  
  Quaternion q = ToQuaternion(goalpose[5], goalpose[4], goalpose[3]);
  float pe0[3] ={goalpose[0],goalpose[1],goalpose[2]}; //position x,y,z 
 // float qe0[4] ={goalpose[3],goalpose[4],goalpose[5],goalpose[6]};//angles of quanternion x,y,z,w
  float qe0[4] ={q.x,q.y,q.z,q.w};//angles of quanternion x,y,z,w

  float pe6[3]={0,0,L6E};
  float p20[3]={0,0,L02};

  float Re0[3][3]={0},p6e0[3]={0},p60[3]={0},p260[3]={0};
  Quaternion_Matrix(qe0,Re0[0]);
  matrix_multiply(Re0[0],pe6,p6e0,3,1,3);
  matrix_minus(pe0,p6e0,p60,1,3);
  matrix_minus(p60,p20,p260,1,3);

  double s =vector_norm(&p260[0],3);
  
  if(s>(L24+L46) )
  {
    std::cout<<"invalid pose command\n";
    return false;
  }

  float tys,tzs;
  rr(p260,&tys,&tzs);
  
  float tp24z0 = 1.0/(2.0 * s) * (L24 * L24-L46*L46 + s*s);
  float tp240[3]={-(float)sqrt(L24*L24 - tp24z0*tp24z0),0,tp24z0};
  float p240[3]={0},temp[3][3]={0},temp1[3][3]={0},temp2[3][3]={0};

  Ryz(tys,tzs,temp1[0]);
  Rz(Tr,temp2[0]);
  matrix_multiply(temp1[0],temp2[0],temp[0],3,3,3);
  matrix_multiply(temp[0],tp240,p240,3,1,3);
  rr(p240,&joints[1],&joints[0]);
  
  float R20[3][3]={0};
  Ryz(joints[1],joints[0],R20[0]);
  float p40[3]={0},p460[3]={0},p462[3]={0};
  matrix_add(p20,p240,p40,3,1);
  matrix_minus(p60,p40,p460,3,1);
  matrix_multiply(p460,R20[0],p462,1,3,3);
  rr(p462,&joints[3],&joints[2]);
  joints[3] = -joints[3];

  float R42[3][3]={0};
  Ryz(-joints[3], joints[2],R42[0]);
  float R40[3][3]={0},p6e4[3]={0};
  matrix_multiply(R20[0],R42[0],R40[0],3,3,3);
  matrix_multiply(p6e0,R40[0],p6e4,1,3,3);
  rr(p6e4,&joints[5],&joints[4]);

  float R64[3][3]={0},R60[3][3]={0},Re6[3][3]={0};
  Ryz(joints[5], joints[4],R64[0]);
  matrix_multiply(R40[0],R64[0],R60[0],3,3,3);
  Transpose(R60[0],temp[0],3,3);
  matrix_multiply(temp[0],Re0[0],Re6[0],3,3,3);
  joints[6] = atan2(Re6[1][0],Re6[0][0]);
  
  return true;
}

