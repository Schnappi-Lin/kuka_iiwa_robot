#include <iiwa_ros_node.h>
#include "Config.h"
#include <iiwa_kinematic.h>
/*********************************
    Initialize
**********************************/
IIWA_ROS_NODE::IIWA_ROS_NODE(){
    //SIMULATOR GAZEBO
    joint_trajectory_pub = iiwa.advertise<trajectory_msgs::JointTrajectory>("/iiwa/PositionJointInterface_trajectory_controller/command",1);

    //REAL ROBOT
    iiwa_pub_Jp = iiwa.advertise<iiwa_msgs::JointPosition>("/iiwa/command/JointPosition", 1);
    iiwa_pub_Cp = iiwa.advertise<geometry_msgs::PoseStamped>("/iiwa/command/CartesianPoseLin", 1);
   
#ifdef Real_Robot
    iiwa_JointV =  iiwa.advertise< iiwa_msgs::JointVelocity>("/iiwa/command/JointVelocity", 1);
    iiwa_CartV  =iiwa.advertise< geometry_msgs::TwistStamped>("/iiwa/command/CartesianVelocity", 1);//12-4

    iiwa_pub_JpV = iiwa.advertise< iiwa_msgs::JointPositionVelocity>("/iiwa/command/JointPositionVelocity", 1);//TODO: NEED TO TEST

    sub_real_cart  = iiwa.subscribe("iiwa/state/CartesianPose", 1,&IIWA_ROS_NODE::Real_CartesianPoseCallback, this);
    sub_real_joint = iiwa.subscribe("/iiwa/state/JointPosition", 1, &IIWA_ROS_NODE::Real_JointStatesCallback,this);
    
    iiwa_SetSpeedOverride = iiwa.serviceClient<iiwa_msgs::SetSpeedOverride>("/iiwa/configuration/setSpeedOverride");
//05/02/2021
    iiwa_SetCartVelocity = iiwa.serviceClient<iiwa_msgs::SetSmartServoLinSpeedLimits>("/iiwa/configuration/setSmartServoLinLimits");

    TEST2=iiwa.advertise< iiwa_msgs::JointPositionVelocity>("/iiwa/command/JointPositionVelocity", 1);//12-4
    pre_t = ros::Time::now();
//-------------------------------------------------------------------------------------------------------------------------
    // before loop, check if publishers or subscribers are there 
    ros::Rate poll_rate(100);                        
    // while(sub_real_cart.getNumPublishers() == 0||sub_real_joint.getNumPublishers()==0) 
    // {
    //     ROS_INFO("waiting publishers from real robot....");
    //     printf("%d, %d \n",sub_real_cart.getNumPublishers(),sub_real_joint.getNumPublishers());
    //     poll_rate.sleep();
    // } 
    //TODO:Initialize all the variables
    SetSpeedOverride(1.0); //set override
    // SetSmartServoSpeedLimit(1); //0-1(check the manual) m/s && rad/s
    // test = iiwa.subscribe("/iiwa/state/latencytest", 1, &IIWA_ROS_NODE::testlatency,this);
    Send_flag=1;
#else
    sub_sim_joint = iiwa.subscribe("iiwa/joint_states", 1, &IIWA_ROS_NODE::Sim_JointStatesCallback,this);
    sub_sim_cart  = iiwa.subscribe("iiwa/SimulatorState/CartesianPose", 1, &IIWA_ROS_NODE::Sim_CartesianPoseCallback,this);
#endif

    //TODO:
    gripper.Init(iiwa); 
    ROS_INFO("iiwa ros node Initialization finish\n");
}

void IIWA_ROS_NODE::testlatency(iiwa_msgs::JointVelocity test_v)
{
    
    if(test_v.header.stamp==latency_frame_check)
    {
        cout<<"ros side is: "<<(ros::Time::now()- begin_t)*1000<<"ms, ";//
        cout<<" robot receive time is: "<<test_v.velocity.a4*1000<<"ms\n";
        // cout<<"time consuming is: "<<(ros::Time::now()- pre_t)*1000<<"ms ";//
        cout<<" robot velocity is: "<<test_v.velocity.a6*1000<<"ms \n robot control loop: " <<test_v.velocity.a5*1000<<"ms \n";//
        // pre_t=ros::Time::now();
        
    }
    else
    {
        cout<<"time stamp is not the same! \n";//
    }
    Send_flag++;
    
}

#define Deg2Rad(x) (x)*(M_PI/180)
#define Rad2Deg(x) (x)*(180/M_PI)
void IIWA_ROS_NODE::IIWA_JointPositionVelocity_Send(float* goal_joints)
{
  iiwa_msgs::JointPositionVelocity msg;

  msg.header.stamp = ros::Time::now();  
  msg.position.a1 = 0;
  msg.position.a2 = 0;
  msg.position.a3 = 0;
  msg.position.a4 = 0;
  msg.position.a5 = 0;
  msg.position.a6 = 0;
  msg.position.a7 = 0;

  msg.velocity.a1 = goal_joints[0];
  msg.velocity.a2 = goal_joints[1];
  msg.velocity.a3 = goal_joints[2];
  msg.velocity.a4 = goal_joints[3];
  msg.velocity.a5 = goal_joints[4];
  msg.velocity.a6 = goal_joints[5];
  msg.velocity.a7 = goal_joints[6];

    if(goal_joints[6]>0.1)
        msg.position.a7 = Deg2Rad(120);
    if(goal_joints[6]<-0.1)
        msg.position.a7 = Deg2Rad(-120);

  TEST2.publish(msg);

}

void IIWA_ROS_NODE::IIWA_JointVelocity_Send(float* goal_joints)
{
  iiwa_msgs::JointVelocity msg;

  msg.header.stamp = ros::Time::now();            
  msg.velocity.a1 = goal_joints[0];
  msg.velocity.a2 = goal_joints[1];
  msg.velocity.a3 = goal_joints[2];
  msg.velocity.a4 = goal_joints[3];
  msg.velocity.a5 = goal_joints[4];
  msg.velocity.a6 = goal_joints[5];
  msg.velocity.a7 = goal_joints[6];

    // if(Send_flag>0)
    {
        
        latency_frame_check =   msg.header.stamp;
        iiwa_JointV.publish(msg);

        begin_t = ros::Time::now();
        Send_flag--;
        counter++;

        // cout<<"lost: "<<Send_flag<<", total: "<<counter<<", sending... \n";
    }
 

}
void IIWA_ROS_NODE::IIWA_Jointposition_Send(float* goal_joints)
{
  iiwa_msgs::JointPosition msg;

  msg.header.stamp = ros::Time::now();            
  msg.position.a1 = goal_joints[0];
  msg.position.a2 = goal_joints[1];
  msg.position.a3 = goal_joints[2];
  msg.position.a4 = goal_joints[3];
  msg.position.a5 = goal_joints[4];
  msg.position.a6 = goal_joints[5];
  msg.position.a7 = goal_joints[6];

  iiwa_pub_Jp.publish(msg);
}
void IIWA_ROS_NODE::IIWA_CartesianVelocity_Send(float* goal_cart)
{
  geometry_msgs::TwistStamped msg;
  msg.header.stamp = ros::Time::now();   
  msg.header.frame_id ="iiwa_link_0"; 
  msg.twist.linear.x = goal_cart[0];
  msg.twist.linear.y = goal_cart[1];
  msg.twist.linear.z = goal_cart[2];
  //q = ToQuaternion(goal_cart[5],goal_cart[4],goal_cart[3]);
  msg.twist.angular.x = goal_cart[3];
  msg.twist.angular.y = goal_cart[4];
  msg.twist.angular.z = goal_cart[5];
//   msg.pose.orientation.z = q.z;
  iiwa_CartV.publish(msg);
}

void IIWA_ROS_NODE::IIWA_Cartesianpose_Send(float* goal_cart)
{
  geometry_msgs::PoseStamped msg;
  Quaternion q;
  msg.header.stamp = ros::Time::now();   
  msg.header.frame_id ="iiwa_link_0"; 
  msg.pose.position.x = goal_cart[0];
  msg.pose.position.y = goal_cart[1];
  msg.pose.position.z = goal_cart[2];
  q = ToQuaternion(goal_cart[5],goal_cart[4],goal_cart[3]);
  msg.pose.orientation.w = q.w;
  msg.pose.orientation.x = q.x;
  msg.pose.orientation.y = q.y;
  msg.pose.orientation.z = q.z;
  iiwa_pub_Cp.publish(msg);
}
// enum Joints_Names{
//     iiwa_joint_1,
//     iiwa_joint_2,
//     iiwa_joint_3,
//     iiwa_joint_4,
//     iiwa_joint_5,
//     iiwa_joint_6,
//     iiwa_joint_7
// };
string GetJointsNames(int index) {
   switch(index) {
      case 1:
         return "iiwa_joint_1";
      case 2:
         return "iiwa_joint_2";
      case 3:
         return "iiwa_joint_3";
      case 4:
         return "iiwa_joint_4";
      case 5:
         return "iiwa_joint_5";
      case 6:
          return "iiwa_joint_6";
      case 7:
          return "iiwa_joint_7";
      default:
         return "Invalid index";
   }
}
void IIWA_ROS_NODE::TrajectoryJoint_Send(float* goal_joints, float duration_t)
{
    trajectory_msgs::JointTrajectory msg_joint; 
    trajectory_msgs::JointTrajectoryPoint point;

    msg_joint.header.stamp = ros::Time::now();
    for(int i=1;i<8;i++){
        msg_joint.joint_names.push_back(GetJointsNames(i));
        point.positions.push_back(goal_joints[i-1]);
    }
    point.time_from_start = ros::Duration(duration_t);
    msg_joint.points.push_back(point);
    joint_trajectory_pub.publish(msg_joint);
}

void IIWA_ROS_NODE::Real_CartesianPoseCallback(iiwa_msgs::CartesianPose cartesian_pose) 
{
    Quaternion q;
    EulerAngles angles;
    q.w = cartesian_pose.poseStamped.pose.orientation.w;
    q.x = cartesian_pose.poseStamped.pose.orientation.x;
    q.y = cartesian_pose.poseStamped.pose.orientation.y;
    q.z = cartesian_pose.poseStamped.pose.orientation.z;
    angles = ToEulerAngles(q);

    cur_cart_pose[0] = cartesian_pose.poseStamped.pose.position.x;
    cur_cart_pose[1] = cartesian_pose.poseStamped.pose.position.y; 
    cur_cart_pose[2] = cartesian_pose.poseStamped.pose.position.z; 
    cur_cart_pose[3] = angles.roll; 
    cur_cart_pose[4] = angles.pitch; 
    cur_cart_pose[5] = angles.yaw; 
}

void IIWA_ROS_NODE::Real_JointStatesCallback(iiwa_msgs::JointPosition joint_states)
{
  real_joint_state.position.clear();
  real_joint_state.position.push_back(joint_states.position.a1);
  real_joint_state.position.push_back(joint_states.position.a2);
  real_joint_state.position.push_back(joint_states.position.a3);
  real_joint_state.position.push_back(joint_states.position.a4);    
  real_joint_state.position.push_back(joint_states.position.a5); 
  real_joint_state.position.push_back(joint_states.position.a6);
  real_joint_state.position.push_back(joint_states.position.a7);
}
void IIWA_ROS_NODE::Sim_JointStatesCallback(sensor_msgs::JointState joint_states)
{
    sim_joint_state = joint_states;
}
void IIWA_ROS_NODE::Sim_CartesianPoseCallback(geometry_msgs::PoseStamped cartesian_pose) 
{
    Quaternion q;
    EulerAngles angles;
    q.w = cartesian_pose.pose.orientation.w;
    q.x = cartesian_pose.pose.orientation.x;
    q.y = cartesian_pose.pose.orientation.y;
    q.z = cartesian_pose.pose.orientation.z;
    angles = ToEulerAngles(q);

    cur_cart_pose[0] = cartesian_pose.pose.position.x;
    cur_cart_pose[1] = cartesian_pose.pose.position.y; 
    cur_cart_pose[2] = cartesian_pose.pose.position.z; 
    cur_cart_pose[3] = angles.roll; 
    cur_cart_pose[4] = angles.pitch; 
    cur_cart_pose[5] = angles.yaw; 
   
}

bool IIWA_ROS_NODE::SetSpeedOverride(float override)
{
    iiwa_msgs::SetSpeedOverride clientSpeed;
    clientSpeed.request.override_reduction =override;
    if(iiwa_SetSpeedOverride.call(clientSpeed)){

        if(clientSpeed.response.success){
            ROS_INFO("successful set SpeedOverride to %f!",override);
            return true;
        }   
        else{
            ROS_INFO("error, fail to set SpeedOverride to %f!",override);
            return false;
        }
    }
    else{
        ROS_ERROR("Oh, SpeedOverride-service could not be called!!!");
        return false;
    }
}

bool IIWA_ROS_NODE::SetSmartServoSpeedLimit(float limit)
{
    iiwa_msgs::SetSmartServoLinSpeedLimits clientlimit;
    clientlimit.request.max_cartesian_velocity.linear.x =limit;
    clientlimit.request.max_cartesian_velocity.linear.y =limit;
    clientlimit.request.max_cartesian_velocity.linear.z =limit;
    clientlimit.request.max_cartesian_velocity.angular.x =0.1;
    clientlimit.request.max_cartesian_velocity.angular.y =0.1;
    clientlimit.request.max_cartesian_velocity.angular.z =0.1;

    if(iiwa_SetCartVelocity.call(clientlimit)){

        if(clientlimit.response.success){
            ROS_INFO("successful set cartesian Lin speed limit to %f!",limit);
           
            return true;
        }   
        else{
            ROS_INFO("error, fail to set cartesian Lin speed limit to %f!",limit);
            return false;
        }
    }
    else{
        ROS_ERROR("Oh, SpeedOverride-service could not be called!!!");
        return false;
    }
    
}