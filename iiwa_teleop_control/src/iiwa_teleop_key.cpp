#include"ros/ros.h"
#include<iostream>
#include<termios.h>   //termios, TSCANOW,ECHO,ICANON
#include <unistd.h>  //STDIN_FILENO
#include <chrono>
#include <string.h>
#include <math.h>
#include<Config.h>

#include <iiwa_ros_node.h>
#include <iiwa_kinematic.h>
using namespace std;
//Joints


#define Joints_P_v  20        // degree/s

// #define Joints_V_init 10    // degree/s

#define Joints_V_MAX  1   // rad/s

//Cartesian
#define Cart_P_v  0.1  // m/s
#define Cart_V_upper_bound 1

#define Cart_V_init 0.1  // m/s
#define Cart_V_step 0.01 // m/s
#define Cart_V_Max  1    // m/s

#define Deg2Rad(x) (x)*(M_PI/180)
#define Rad2Deg(x) (x)*(180/M_PI)

enum Space{
    JOINT_SPACE,
    CARTESIAN_SPACE,
};
enum Mode{
    POSITION_MODE,
    VELOCITY_MODE
};
enum JointsObject{
  A1=0,
  A2,
  A3,
  A4,
  A5,
  A6,
  A7
};
enum CartesianObject{
    x=0,
    y,
    z,
    Roll,Pitch,Yaw,
    All
};
struct Control_State{
  Space space;
  Mode mode;
  JointsObject joints_object;
  CartesianObject cart_object;
};
enum Send_Command{
  Send_Joints_Position,
  Send_Joints_Velocity,
  Send_Cartesian_Pose,
  Send_Cartesian_Velocity,
  Gripper_open,
  Gripper_close,
  Stop,
  Not_Send
};
struct Goal_State{
  Send_Command send_cmd;
  float joints_goal[7];
  float joints_velocity[7];
  float cart_goal[6];
  float cart_velocity[6];
};



class iiwa_keyboard_controler
{
  private:
    const float jointlimit[7]={168,118,168,118,168,118,173};
    // float cur_cartesian_pose[6]; 
    int pre_key=0;
    float cur_joint_v=0;
    float cur_cart_v = Cart_V_init;
    IIWA_ROS_NODE* iiwa_ros_node;
    double Joints_Step_size; // Degree
    double Cart_Step_size; //linear:m  angle:rad
    
  public:
    Control_State control_state;
    Goal_State goal_state;
    float speedlimit = 0;
    bool CartesianMax_SpeedLimit_change;
    float duration_t=0;

   //class init
  iiwa_keyboard_controler(IIWA_ROS_NODE* n){ 
    iiwa_ros_node = n;
    control_state.space = JOINT_SPACE;
    control_state.mode = POSITION_MODE;
    control_state.joints_object = A1;
    control_state.cart_object = x;

    goal_state.send_cmd = Not_Send;
    Goal_Joint_Velocity_reset();
    Goal_Cart_Velocity_reset();

    CartesianMax_SpeedLimit_change=0;
    Joints_Step_size = Default_Joints_Step_size;
    Cart_Step_size = Default_Cart_Step_size;
    };

  string getNameofControlledObject()
  {
    string ss;
    switch(control_state.space)
    {
      case JOINT_SPACE:
        ss ="Joint_A"+ to_string(control_state.joints_object+1);
        break;

      case CARTESIAN_SPACE:
        switch(control_state.cart_object)
        {
          case x: ss= "x-axis direction";break;
          case y: ss= "y-axis direction";break;
          case z: ss= "z-axis direction";break;
          case Roll: ss= "Roll orientation";break;
          case Pitch: ss= "Pitch orientation";break;
          case Yaw: ss= "Yaw orientation";break;
          default: ss="cartesian degree is not intial before using";
        }
        break;
      default:
          ss="something need to be Intial !";
    }
    return ss;
  }
  
  void Update_CurJoints(){
      
        for(int i =0;i<7;i++){
          #ifdef Real_Robot 
            goal_state.joints_goal[i] = iiwa_ros_node->real_joint_state.position[i]; 
          #else
            goal_state.joints_goal[i] =  iiwa_ros_node->sim_joint_state.position[i];
          #endif 
        }
     
  }
  
  void Update_CurCartPose(CartesianObject object){
      if(object==All){
        for(int i=0;i<6;i++)
        {
          goal_state.cart_goal[i]=iiwa_ros_node->cur_cart_pose[i];
        }
      }else{
          goal_state.cart_goal[object]=iiwa_ros_node->cur_cart_pose[object];
      }
    
  }
  
  void PrintCurrentPistion(int joint_or_cartpose){
    if(joint_or_cartpose){ //print cartesianPose: 
        cout<<"current cartesion pose are: \n";
        cout<<"x : "<<iiwa_ros_node->cur_cart_pose[0]<<"m\n";
        cout<<"y : "<<iiwa_ros_node->cur_cart_pose[1]<<"m\n";
        cout<<"z : "<<iiwa_ros_node->cur_cart_pose[2]<<"m\n";
        cout<<"Roll : "<<Rad2Deg(iiwa_ros_node->cur_cart_pose[3]) <<" degree\n";
        cout<<"Pitch : "<<Rad2Deg(iiwa_ros_node->cur_cart_pose[4])<<" degree\n";
        cout<<"Yaw : "<<Rad2Deg(iiwa_ros_node->cur_cart_pose[5])<<" degree\n";
    }else 
    {
      cout<<"current joint angles are: \n";//print joints angle in degree
      for(int i =0;i<7;i++){
          #ifdef Real_Robot 
            cout<<"Joint A"<<i+1<<": "<< Rad2Deg(iiwa_ros_node->real_joint_state.position[i])<<" degree\n";
          #else
            cout<<"Joint A"<<i+1<<": "<< Rad2Deg(iiwa_ros_node->sim_joint_state.position[i])<<" degree\n";
          #endif 
        }
    }
  }
  
  float Joints_PositionLimit_Check(float value_in_rad)
  {
    int object = control_state.joints_object;
      if( value_in_rad >=Deg2Rad(jointlimit[object]))
          return Deg2Rad(jointlimit[object]);
     else if (value_in_rad <=Deg2Rad(-jointlimit[object]))
        return Deg2Rad(-jointlimit[object]);
      else 
        return value_in_rad;
  }
  
  float Joints_Velocitylimit_Check(float vel_in_rad)
  {
    if(vel_in_rad>=Joints_V_MAX)
    {
       return Joints_V_MAX;
    }
    else if(vel_in_rad<0)
    {
      return 0;
    }          
    else
    {
      return vel_in_rad; 
    }
            
      
  }
 
  void Goal_Joint_Velocity_reset(){
    for(int i=0;i<7;i++)
        goal_state.joints_velocity[i]=0;
  }
  
  void Goal_Cart_Velocity_reset(){
    for(int i=0;i<6;i++)
        goal_state.cart_velocity[i]=0;
  }
  
  void Joints_Position_Control(int key)
  {
    int object = control_state.joints_object;
    switch (key)
    {    
      
      case 'w': case 'W':
            Update_CurJoints();
            goal_state.joints_goal[object]= Joints_PositionLimit_Check( goal_state.joints_goal[object]+Deg2Rad(Joints_Step_size) );
            duration_t= Joints_Step_size/Joints_P_v;//this parameter only use for gazebo simulator
            cout<<"goal posistion is : "<< Rad2Deg(goal_state.joints_goal[object])<<"\n";
            goal_state.send_cmd = Send_Joints_Position;
            break; 
      case 's': case 'S':
            Update_CurJoints();
            goal_state.joints_goal[object]= Joints_PositionLimit_Check( goal_state.joints_goal[object]-Deg2Rad(Joints_Step_size));
            duration_t= Joints_Step_size/Joints_P_v;//this parameter only use for gazebo simulator
            cout<<"goal posistion is : "<< Rad2Deg(goal_state.joints_goal[object])<<"\n"; 
            goal_state.send_cmd = Send_Joints_Position;
            break;  

      case '1':case '2':case '3': case '4': case '5':case '6': case '7': // joint selection
            control_state.joints_object = JointsObject(key -'1');
            cout<<"current controlled joint is:" << getNameofControlledObject()<<"\n";
            break; 
      case '+':
              Joints_Step_size = Joints_Step_size+Joint_step_step_size;
              cout<< "current step size is: "<<Joints_Step_size<<" degree \n"; 
              break;
      case '-':
              Joints_Step_size = (Joints_Step_size-Joint_step_step_size<0)? Joints_Step_size:(Joints_Step_size-Joint_step_step_size);
              cout<< "current step size is: "<<Joints_Step_size<<" degree \n"; 
              break;
      case 0: case -1://uncertain state(either no key pressed or key keep pressing)
            break;    
      default:
        cout<<"unvalid command in JointPosition Mode\n";
      break;
    }
  }

//STOP has problem: when speed high and keep pressing , the robot will not stop immediately
  void Joints_Velocity_Control(int key)
  {
    int object = control_state.joints_object;
      switch (key)
      {    
        case 0: //no key pressed
              if(pre_key==-1)
              {
                goal_state.joints_velocity[object] = 0;
                goal_state.send_cmd = Send_Joints_Velocity;
                cout<< "stop moving\n "; 
              }
              break;
        case '+':
              cur_joint_v = Joints_Velocitylimit_Check(cur_joint_v+Joints_Vel_step);
              cout<< "joint A"<<object<<" velocity is: "<<Rad2Deg(cur_joint_v) <<" degree/s \n"; 
              break;
        case '-':
              cur_joint_v = Joints_Velocitylimit_Check(cur_joint_v-Joints_Vel_step);
              cout<< "joint A"<<object<<" velocity is: "<<Rad2Deg(cur_joint_v) <<" degree/s \n";  
              break;
        case 'w': case 'W':
              goal_state.joints_velocity[object] = cur_joint_v;
              goal_state.send_cmd = Send_Joints_Velocity;
              cout<< "joint A"<<object<<" velocity is: "<<Rad2Deg(cur_joint_v) <<" degree/s \n"; 
              break;          
        case 's': case 'S':
              goal_state.joints_velocity[object] = -cur_joint_v;
              goal_state.send_cmd = Send_Joints_Velocity;
              cout<< "joint A"<<object<<" velocity is: "<<Rad2Deg(cur_joint_v) <<" degree/s \n";  
              break;     
        case '1':case '2':case '3': case '4': case '5':case '6': case '7': // joint selection under Joint space
              control_state.joints_object = JointsObject(key -'1');
              Goal_Joint_Velocity_reset();
              cur_joint_v=0;
              cout<<"select joint: " << getNameofControlledObject()<<", velocity reset to 0\n";
              break;     
        case -1://uncertain state(either no key pressed or key keep pressing)
              break;
        default:
              cout<<"unvalid command in Joints Velocity Mode\n";
              break;
      }
  }
  
  void CartPostionChange(CartesianObject object,double diff)
  {
    Update_CurCartPose(object);
    goal_state.cart_goal[object] =goal_state.cart_goal[object] + diff;

    if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){ 
        goal_state.cart_goal[object] -=diff;                
        cout<<"limits Hit!!! "<<endl;                   
    }
    else{
        goal_state.send_cmd = Send_Cartesian_Pose;
        duration_t= abs(goal_state.cart_goal[object]-iiwa_ros_node->cur_cart_pose[object])/Cart_P_v;//simulator                   
    } 
  }
  void Cartesian_Position_Control(int key)
  {
      switch (key)
      {
        case '+':
              Cart_Step_size +=Cart_step_step_size; 
              cout<< "current step size is: "<<Cart_Step_size<<"m in linear or "<<Rad2Deg(Cart_Step_size) <<" degree in orient"<<" \n"; 
              break;
        case '-':
              Cart_Step_size =(Cart_Step_size-Cart_step_step_size<0)? Cart_Step_size:(Cart_Step_size-Cart_step_step_size);
              cout<< "current step size is: "<<Cart_Step_size<<"m in linear or "<<Rad2Deg(Cart_Step_size) <<" degree in orient"<<" \n"; 
              break;
        case 'w': case 'W'://forward
              CartPostionChange(x, Cart_Step_size);
              break;         
        case 's': case 'S'://backward
              CartPostionChange(x, -Cart_Step_size);
              break;  
        case 'a':case 'A'://left
              CartPostionChange(y, Cart_Step_size);
              break;
        case 'd':case 'D'://right
              CartPostionChange(y, -Cart_Step_size);
              break;
        case 'q':case 'Q'://up
              CartPostionChange(z, Cart_Step_size);
              break;
        case 'e':case 'E'://down
              CartPostionChange(z, -Cart_Step_size);
              break;
        case 'R':case 'r': //roll +
              CartPostionChange(Roll, Cart_Step_size);
              break;
        case 'f':case 'F': //roll -
              CartPostionChange(Roll, -Cart_Step_size);
              break;
        case 't':case 'T': //pitch +
              CartPostionChange(Pitch, Cart_Step_size);
              break;
        case 'g':case 'G': //pitch -
              CartPostionChange(Pitch, -Cart_Step_size);
              break;
        case 'Y':case 'y': //yaw +
              CartPostionChange(Yaw, Cart_Step_size);
              break;
        case 'H':case 'h': //yaw -
              CartPostionChange(Yaw, -Cart_Step_size);
              break;
        case -1:case 0:
              break;
        default:
              cout<<"unvalid command in Cartesian Postion Mode\n";
              break;
      }
  }
  void Cartesian_Velocity_Control(int key)
  {
     int object = control_state.cart_object;
      
      switch (key)
      {
        case 0: //no input
              if(pre_key!=key)
                  {  
                    // #ifdef Real_Robot
                    //     Update_CurJoints();   
                    //     goal_state.send_cmd = Send_Joints;                            
                    //     duration_t=0.05; 
                    //     cur_cart_v = Cart_V_init;  
                    // #endif               
                  }  
              break;
        case '+':
              speedlimit+=0.1*Cart_V_upper_bound;
              CartesianMax_SpeedLimit_change=1;
              if(speedlimit>Cart_V_upper_bound){
                speedlimit=Cart_V_upper_bound;
                CartesianMax_SpeedLimit_change=0;
              }
              // SetSmartServoSpeedLimit(speedlimit); //0-1(check the manual) m/s && rad/s
              cout<< "current velocity limit is"<<speedlimit<<" m/s \n"; 
              break;
        case '-':
              speedlimit-=0.1*Cart_V_upper_bound;
              CartesianMax_SpeedLimit_change=1;
              if(speedlimit<0){
                   speedlimit=0;
                   CartesianMax_SpeedLimit_change=0;
              }
              // SetSmartServoSpeedLimit(speedlimit); //0-1(check the manual) m/s && rad/s
              cout<< "current velocity limit is"<<speedlimit<<" m/s \n"; 
              break;
         
        case 'w': case 'W'://forward
                Update_CurCartPose(x);
                goal_state.cart_goal[x] =goal_state.cart_goal[x] + Cart_Step_size;

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){ //TODO: check the link length is correct or not
                    goal_state.cart_goal[x] -=Cart_Step_size;                
                    cout<<"limits Hit!!! "<<endl;                   
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[x]-iiwa_ros_node->cur_cart_pose[x])/Cart_P_v;//simulator                   
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[x]<<"\n";
            break;         
        case 's': case 'S'://backward
                Update_CurCartPose(x);
                goal_state.cart_goal[x] =goal_state.cart_goal[x] - Cart_Step_size;    

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){
                    goal_state.cart_goal[x] +=Cart_Step_size;                
                    cout<<"limits Hit!!!"<<endl;                  
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[x]-iiwa_ros_node->cur_cart_pose[x])/Cart_P_v;                  
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[x]<<"\n";
            break;  
        case 'a':case 'A'://left
                Update_CurCartPose(y);
                goal_state.cart_goal[y] =goal_state.cart_goal[y] + Cart_Step_size;    

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){
                    goal_state.cart_goal[y] -=Cart_Step_size;                
                    cout<<"limits Hit!!!"<<endl;                  
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[y]-iiwa_ros_node->cur_cart_pose[y])/Cart_P_v;                  
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[y]<<"\n";
                break;
         case 'd':case 'D'://right
                Update_CurCartPose(y);
                goal_state.cart_goal[y] =goal_state.cart_goal[y] - Cart_Step_size;    

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){
                    goal_state.cart_goal[y] +=Cart_Step_size;                
                    cout<<"limits Hit!!!"<<endl;                  
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[y]-iiwa_ros_node->cur_cart_pose[y])/Cart_P_v;                  
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[y]<<"\n";
                break;
         case 'q':case 'Q'://up
                Update_CurCartPose(z);
                goal_state.cart_goal[z] =goal_state.cart_goal[z] + Cart_Step_size;    

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){
                    goal_state.cart_goal[z] -=Cart_Step_size;                
                    cout<<"limits Hit!!!"<<endl;                  
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[z]-iiwa_ros_node->cur_cart_pose[z])/Cart_P_v;                  
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[z]<<"\n";
                break;
        case 'e':case 'E'://down
                Update_CurCartPose(z);
                goal_state.cart_goal[z] =goal_state.cart_goal[z] - Cart_Step_size;    

                if(!Inverse_Solver(goal_state.cart_goal,goal_state.joints_goal)){
                    goal_state.cart_goal[z] +=Cart_Step_size;                
                    cout<<"limits Hit!!!"<<endl;                  
                }
                else{
                    goal_state.send_cmd = Send_Cartesian_Pose;
                    duration_t= abs(goal_state.cart_goal[z]-iiwa_ros_node->cur_cart_pose[z])/Cart_P_v;                  
                } 
                cout<<"current cartesian "<<getNameofControlledObject()<<" is: "<<goal_state.cart_goal[z]<<"\n";
                break;
        //  case 'x': case 'y': case 'z': 
        //       control_state.cart_object= CartesianObject(key-'x');
        //       cout<<"Controlling "<<getNameofControlledObject()<<"\n";
        //       break;

          case 'R': //roll
              control_state.cart_object = Roll;
              cout<<"Controlling "<<getNameofControlledObject()<<"\n";
              break;
          case 'B': //pitch 
              control_state.cart_object = Pitch;
              cout<<"Controlling "<<getNameofControlledObject()<<"\n";
              break;
          case 'Y': //yaw 
              control_state.cart_object = Yaw;
              cout<<"Controlling "<<getNameofControlledObject()<<"\n";
              break;
          case -1:
              break;
          default:
            cout<<"unvalid command in Cartesian Space\n";
          break;
        }
  }
  
  void Joints_Control_Mode(int key)
  {
      switch (control_state.mode)
      {    
        case POSITION_MODE:
              Joints_Position_Control(key);
              break; 
        case VELOCITY_MODE:
              Joints_Velocity_Control(key);
              break;     
        default:
          cout<<"some error happen\n";
        break;
      }
  }
  void Cartesian_Control_Mode(int key)
  {
      switch (control_state.mode)
      {    
        case POSITION_MODE:
              Cartesian_Position_Control(key);
              break; 
        case VELOCITY_MODE:
              Cartesian_Velocity_Control(key);
              break;     
        default:
          cout<<"some error happen\n";
        break;
      }
  }
  void ControlMode_Updata(int key)
  {
      static Control_State pre_control_state;
      switch(key)
      {
        case 'J': //Joint space
              control_state.space = JOINT_SPACE;
              control_state.mode = POSITION_MODE;
              control_state.joints_object = A1;
              iiwa_ros_node->SetSpeedOverride(1.0); 
              Joints_Step_size = Default_Joints_Step_size;
              cout<<"Joint Position control mode, current controlled joint is:"<<getNameofControlledObject()
                  <<", step size is:"<<Joints_Step_size<<" degree, speedoverride = 1.0"<<endl;
            break;
        case 'C': //Cartesian space
            control_state.space = CARTESIAN_SPACE;
            control_state.mode = POSITION_MODE;
            Update_CurCartPose(All);
            Cart_Step_size = Default_Cart_Step_size;
            iiwa_ros_node->SetSpeedOverride(1.0); 
            cout<<"Cartesian Position mode,set speedovrride 1.0, the step size is: "<< Cart_Step_size<<endl;
            break;
        case 'V': // velocity control 
            if(control_state.mode == VELOCITY_MODE){
              cout<<"You are already in Velocity control Mode, nothing changed.\n";
            }
            else{
              control_state.mode = VELOCITY_MODE;
              control_state.joints_object = A1;
              Goal_Joint_Velocity_reset();
              cur_joint_v=0;    //Initialize velocity to 0
              iiwa_ros_node->SetSpeedOverride(0.5); //for safety reason
              cout<<"switch to Velocity Control mode, the controlled joint is:"<<getNameofControlledObject()
                  <<", reset jointvelocity to 0 \n";
            }
            break;
        case 'P': case 'p': 
            PrintCurrentPistion(0); //print joint angle
            PrintCurrentPistion(1); //print cartesian pose
            break;
        case 'o': case 'O':
            goal_state.send_cmd = Gripper_open;
            break;
        case 'l': case 'L':
            goal_state.send_cmd = Gripper_close;
            break;
        
        default:
            switch (control_state.space)
            {
              case JOINT_SPACE:
                Joints_Control_Mode(key);
                break;
              case CARTESIAN_SPACE:
                Cartesian_Control_Mode(key);
                break;
              default:
                break;
            }
            break;
      }
      pre_key=key;
      pre_control_state=control_state;
  }
  
};

/*
    get keyboard directly without waiting for 'Enter'
*/
int getch()
{
    static struct termios oldt,newt;
   // static ros::Time begin,end;
    static int pre_c=0, counter=0,temp_c=0;
    int c;
    /*tcgetattr gets the parameters of the current terminal
    STDIN_FILENO will tell tcgetattr that it should write the settings
    of stdin to oldt*/
    tcgetattr(STDIN_FILENO, &oldt);

    newt = oldt;                               //copy the setting

    /*ICANON normally takes care that one line at a time will be processed
    that means it will return if it sees a "\n" or an EOF or an EOL*/
    newt.c_lflag &= ~(ICANON|ECHO);                 // disable buffering
    
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings
    
    struct timeval timeout={0, 1000};//33*1000
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO,&fds);
    int ret = select(1,&fds,NULL,NULL,&timeout);
    if (ret == -1) {
        c=-1;
        printf("Oops! ending...\n");
    } else if (ret == 0) {
        c=-1; // time out (unsure state <-- whether no key pressed or keep pressing)
        counter++;
        if(counter>=100) //100
        {
          c=0; //no keyboard input
          // printf("last times of timeout are: %d\n",counter);
          // if(pre_c!=c)
          //   printf("no keyboard input\n");
        }    
       
    } else {
        c = getchar(); 
        // printf("The %d th enter is: %d\n",temp_c++ ,c);
        // if(counter>0)
        //   printf("last times of timeout are: %d\n",counter);
        counter=0;
    }

    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
    pre_c = c;
    return c;
}


int main(int argc, char **argv)
{
  int key,joint_num=7,count_flag=0,stop_flag=0;

  ros::init(argc, argv, "keyboard_controller");  
  
  IIWA_ROS_NODE iiwa_ros_node;
  iiwa_keyboard_controler iiwa_key_controller(&iiwa_ros_node);
  
  ROS_INFO("start reading keyboard....");

  float Va[6]={0};
  ros::Rate poll_ratet(2);
  poll_ratet.sleep();//for stablize connection

  // using wire 100Hz without lossing  (1-2 ms for transmit(send+back))
  // using wireless, 30hz-40hz will be good (3-12ms for transmit(send+back))
  ros::Rate poll_rate(30); 
  while (ros::ok())
  {
    key = getch();   // get the input from keyboard
    
    ros::spinOnce();  //renew the Current_cartesian_pose
    // printf("key is %d\n",key);
    iiwa_key_controller.ControlMode_Updata(key);  //commad process
    
   
    if(iiwa_key_controller.CartesianMax_SpeedLimit_change){
      iiwa_ros_node.SetSmartServoSpeedLimit(iiwa_key_controller.speedlimit); //0-1(check the manual) m/s && rad/s
      iiwa_key_controller.CartesianMax_SpeedLimit_change=0;
    }
//--------------end test-----------------------------
    switch (iiwa_key_controller.goal_state.send_cmd)
    {
      case Send_Joints_Position:
          iiwa_ros_node.IIWA_Jointposition_Send(iiwa_key_controller.goal_state.joints_goal);
          iiwa_ros_node.TrajectoryJoint_Send(iiwa_key_controller.goal_state.joints_goal,iiwa_key_controller.duration_t);
          iiwa_key_controller.goal_state.send_cmd = Not_Send;
          break;
      case Send_Joints_Velocity:
          //under this mode there is no such controller simulator in gazebo
          //TODO: add velocity controller in gazebo
          iiwa_ros_node.IIWA_JointVelocity_Send(iiwa_key_controller.goal_state.joints_velocity);
          iiwa_key_controller.goal_state.send_cmd = Not_Send;  
          break;
      case Send_Cartesian_Pose:
          iiwa_ros_node.IIWA_Cartesianpose_Send(iiwa_key_controller.goal_state.cart_goal); 
          iiwa_ros_node.TrajectoryJoint_Send(iiwa_key_controller.goal_state.joints_goal,iiwa_key_controller.duration_t);
          iiwa_key_controller.goal_state.send_cmd = Not_Send;
          break;
      case Send_Cartesian_Velocity:
          //under this mode there is no such controller simulator in gazebo
          // TODO: add velocity controller in gazebo
          iiwa_ros_node.IIWA_CartesianVelocity_Send(iiwa_key_controller.goal_state.cart_velocity);
          iiwa_key_controller.goal_state.send_cmd = Not_Send;
          break;
      case Gripper_open:
          iiwa_ros_node.begin_t = ros::Time::now();
          if(iiwa_ros_node.gripper.opengripper())
          {
             iiwa_key_controller.goal_state.send_cmd = Not_Send;
             cout<<"time consuming is: "<<ros::Time::now()-iiwa_ros_node.begin_t<<"\n";//250ms-500ms
          }
          break;
      case Gripper_close:
          iiwa_ros_node.begin_t = ros::Time::now(); 
          if(iiwa_ros_node.gripper.closegripper())
          {
              iiwa_key_controller.goal_state.send_cmd = Not_Send;
              cout<<"time consuming is: "<<ros::Time::now()-iiwa_ros_node.begin_t<<"\n"; // 250ms-500ms
          }
          break;
      case Stop:
          iiwa_ros_node.IIWA_JointVelocity_Send(Va);
          break;
      default:
        break;
    }
    // poll_rate.sleep();
  }
    return 0;
}